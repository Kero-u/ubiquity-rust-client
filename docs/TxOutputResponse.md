# TxOutputResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | Option<**i32**> | the output index within a given transaction | [optional]
**tx_id** | Option<**String**> | the transaction identifier | [optional]
**date** | Option<**i32**> | the transaction creation unix timestamp | [optional]
**block_id** | Option<**String**> | the identifier of the block which the transaction was mined | [optional]
**block_number** | Option<**i32**> | the number of the block which the transaction was mined | [optional]
**confirmations** | Option<**i32**> | the number of confirmations the transaction took in order to be mined | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


