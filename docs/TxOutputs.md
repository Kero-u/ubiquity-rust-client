# TxOutputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | Option<**i32**> | Number of items in block identifiers | [optional]
**continuation** | Option<[**crate::models::TxPageContinuation**](tx_page_continuation.md)> |  | [optional]
**data** | Option<[**Vec<crate::models::TxOutputsData>**](tx_outputs_data.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


