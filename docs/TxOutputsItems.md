# TxOutputsItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | Option<**String**> | the status of the given transaction output | [optional]
**is_spent** | Option<**bool**> | whether the transaction output was spent or not | [optional]
**spent** | Option<[**crate::models::TxOutputResponse**](tx-output-response.md)> |  | [optional]
**value** | Option<**i32**> | the amount of tokens within the given output | [optional]
**mined** | Option<[**crate::models::TxOutputResponse**](tx-output-response.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


