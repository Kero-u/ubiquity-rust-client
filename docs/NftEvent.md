# NftEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**contract_address** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**event_type** | Option<**String**> |  | [optional]
**timestamp** | Option<**i64**> |  | [optional]
**from_account** | Option<**String**> |  | [optional]
**to_account** | Option<**String**> |  | [optional]
**quantity** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


