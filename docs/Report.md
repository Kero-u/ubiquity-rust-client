# Report

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**Vec<crate::models::ReportField>**](report_field.md) | Transaction items | 
**items** | **i32** | The number of transactions in the report | 
**limit** | Option<**i32**> | The limit number provided in the request or the default | [optional]
**continuation** | Option<**String**> | Continuation token to send in the next request if there are more items | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


