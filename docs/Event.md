# Event

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | Option<**i32**> |  | [optional]
**block_id** | Option<**String**> |  | [optional]
**block_number** | Option<**i32**> |  | [optional]
**date** | Option<**i64**> |  | [optional]
**decimals** | Option<**i32**> |  | [optional]
**denomination** | Option<**String**> |  | [optional]
**destination** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**meta** | Option<[**serde_json::Value**](.md)> |  | [optional]
**source** | Option<**String**> |  | [optional]
**transaction_id** | Option<**String**> |  | [optional]
**_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


