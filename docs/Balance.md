# Balance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | Option<[**crate::models::Currency**](currency.md)> |  | [optional]
**confirmed_balance** | Option<**String**> |  | [optional]
**pending_balance** | Option<**String**> |  | [optional]
**confirmed_nonce** | Option<**i32**> |  | [optional]
**confirmed_block** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


