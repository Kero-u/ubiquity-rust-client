# GetAssetResponseAsset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_id** | Option<**String**> |  | [optional]
**token_type** | Option<**String**> |  | [optional]
**media** | Option<[**crate::models::GetAssetResponseAssetMedia**](GetAssetResponse_AssetMedia.md)> |  | [optional]
**name** | Option<**String**> |  | [optional]
**mint_date** | Option<**i64**> |  | [optional]
**token_uri** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**contract_address** | Option<**String**> |  | [optional]
**wallets** | Option<[**Vec<crate::models::GetAssetResponseAssetWallet>**](GetAssetResponse_AssetWallet.md)> |  | [optional]
**attributes** | Option<[**Vec<crate::models::GetAssetResponseAssetTrait>**](GetAssetResponse_AssetTrait.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


