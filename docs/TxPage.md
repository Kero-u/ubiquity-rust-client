# TxPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | Option<**i32**> | Number of items in txs | [optional]
**items** | Option<[**Vec<crate::models::Tx>**](tx.md)> |  | [optional]
**continuation** | Option<[**crate::models::TxPageContinuation**](tx_page_continuation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


